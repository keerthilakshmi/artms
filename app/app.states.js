altairApp
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider',
        function($stateProvider, $urlRouterProvider, $locationProvider) {

            $locationProvider.hashPrefix('');

            // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
            $urlRouterProvider
                .when('/dashboard', '/')
                .otherwise('/');

            $stateProvider
            // -- ERROR PAGES --
                .state("error", {
                    url: "/error",
                    templateUrl: 'app/views/error.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit'
                            ]);
                        }]
                    }
                })
                .state("error.404", {
                    url: "/404",
                    templateUrl: 'app/components/pages/error_404View.html'
                })
                .state("error.500", {
                    url: "/500",
                    templateUrl: 'app/components/pages/error_500View.html'
                })

            //Recent Hotels
            .state("restricted.recenthotels", {
                url: "/recenthotels",
                templateUrl: 'app/components/Recenthotel/recenthotelsView.html',
                controller: 'recenthotelsCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'bower_components/angular-resource/angular-resource.min.js',
                            'lazy_uikit',
                            'lazy_iCheck',
                            'lazy_parsleyjs',
                            'lazy_selectizeJS',
                            'lazy_datatables',
                            'lazy_character_counter',
                            'app/components/Recenthotel/recenthotelsCtrl.js'
                        ]);
                    }],

                },
                data: {
                    pageTitle: 'Recent Hotels'
                }
            })


            //  Guests in Hotel
            .state("restricted.guestshotel", {
                url: "/guestshotel",
                templateUrl: 'app/components/Guests/guestshotelView.html',
                controller: 'GuestshotelCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'bower_components/angular-resource/angular-resource.min.js',
                            'lazy_parsleyjs',
                            'lazy_dropify',
                            'lazy_datatables',
                            'lazy_character_counter',
                            'app/components/Guests/guestshotelCtrl.js'

                        ]);
                    }]
                },
                data: {
                    pageTitle: 'Guests Hotel'
                }
            })

             //  Map in Hotel
            .state("restricted.Maphotel", {
                url: "/Maphotel",
                templateUrl: 'app/components/Map/mapView.html',
                controller: 'MapCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'bower_components/angular-resource/angular-resource.min.js',
                            'lazy_parsleyjs',
                            'lazy_dropify',
                            'lazy_datatables',
                            'lazy_character_counter',
                            'app/components/Map/mapCtrl.js'

                        ]);
                    }]
                },
                data: {
                    pageTitle: 'Map Hotel'
                }
            })

            // Login
            .state("login1", {
                url: "/",
                templateUrl: 'app/components/Login/login1View.html',
                controller: 'loginCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'lazy_uikit',
                            'lazy_iCheck',
                             'lazy_parsleyjs',
                             'lazy_selectizeJS',
                            'app/components/Login/login1Controller.js'
                        ]);
                    }]
                }
            })

            // -- RESTRICTED --
            .state("restricted", {
                    abstract: true,
                    url: "",
                    templateUrl: 'app/views/restricted.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_selectizeJS',
                                'lazy_switchery',
                                'lazy_prismJS',
                                'lazy_autosize',
                                'lazy_iCheck',
                                'lazy_themes'
                            ]);
                        }]
                    }
                })

                // // -- DASHBOARD --
                // .state("restricted.dashboard", {
                //     url: "/dashboard",
                //     templateUrl: 'app/components/Guests/dashboardview.html',
                //     controller: 'dashboardCtrls',
                //     resolve: {
                //         deps: ['$ocLazyLoad', function($ocLazyLoad) {
                //             return $ocLazyLoad.load([
                //                 // ocLazyLoad config (app/app.js)
                //                 'lazy_countUp',
                //                 'lazy_charts_peity',
                //                 'lazy_charts_easypiechart',
                //                 'lazy_charts_metricsgraphics',
                //                 'lazy_charts_chartist',
                //                 'lazy_weathericons',
                //                 'lazy_clndr',
                //                 'lazy_google_maps',
                //                 'app/components/Guests/dashboardCtrl.js'
                //             ], {
                //                 serie: true
                //             });
                //         }],
                //         sale_chart_data: function($http) {
                //             return $http({
                //                     method: 'GET',
                //                     url: 'data/mg_dashboard_chart.min.json'
                //                 })
                //                 .then(function(data) {
                //                     return data.data;
                //                 });
                //         },
                //         user_data: function($http) {
                //             return $http({
                //                     method: 'GET',
                //                     url: 'data/user_data.json'
                //                 })
                //                 .then(function(data) {
                //                     return data.data;
                //                 });
                //         }
                //     },
                //     data: {
                //         pageTitle: 'Dashboard'
                //     },
                //     ncyBreadcrumb: {
                //         label: 'Home'
                //     }
                // })


            //  dashboard
            .state("restricted.dashboard", {
                url: "/",
                templateUrl: 'app/components/dashboard/dashboardView.html',
                controller: 'DashboardCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'lazy_uikit',
                            'lazy_iCheck',
                            'lazy_parsleyjs',
                            'lazy_selectizeJS',
                                'lazy_uiSelect',
                            'lazy_datatables',
                            'lazy_character_counter',
                            'app/components/dashboard/dashboardController.js'
                        ]);
                    }]
                },
                data: {
                    pageTitle: ' Detailed Info '
                }
            })
            // artms
                    .state("restricted.addguests", {
                    url: "/addguests",
                    templateUrl: 'app/components/artms/addguestsView.html',
                    controller: 'addguestsCtrl',
                    resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {

                    return $ocLazyLoad.load([
                    'lazy_dropify',
                    'lazy_uikit',
                    'lazy_iCheck',
                    'lazy_parsleyjs',
                    'lazy_datatables',
                    'lazy_wizard',
                    'lazy_KendoUI',
                    'app/components/artms/addguestsController.js'
                    ], {serie:true} );

                    }]
                    },
                    data: {
                    pageTitle: 'Add Guests'
                    }
                    })

        }
    ]);
angular
    .module('altairApp')
    .controller('loginCtrl', [
        '$scope',
        '$rootScope',
        'utils',
        '$window',
        '$http',
        '$state',
        function ($scope, $rootScope, utils, $window, $http, $state) {

            $scope.registerFormActive = false;

            var $login_card = $('#login_card'),
                $login_form = $('#login_form'),
                $login_help = $('#login_help'),
                $register_form = $('#register_form'),
                $login_password_reset = $('#login_password_reset');

            // show login form (hide other forms)
            var login_form_show = function () {
                $login_form
                    .show()
                    .siblings()
                    .hide();
            };

            // show register form (hide other forms)
            var register_form_show = function () {
                $register_form
                    .show()
                    .siblings()
                    .hide();
            };

            // show login help (hide other forms)
            var login_help_show = function () {
                $login_help
                    .show()
                    .siblings()
                    .hide();
            };

            //show password reset form(hide other forms)
            var password_reset_show = function () {
                $login_password_reset
                    .show()
                    .siblings()
                    .hide();
            };

            $scope.loginHelp = function ($event) {
                $event.preventDefault();
                utils.card_show_hide($login_card, undefined, login_help_show, undefined);
            };

            $scope.backToLogin = function ($event) {
                $event.preventDefault();
                $scope.registerFormActive = false;
                utils.card_show_hide($login_card, undefined, login_form_show, undefined);
            };

            $scope.registerForm = function ($event) {
                $event.preventDefault();
                $scope.registerFormActive = true;
                utils.card_show_hide($login_card, undefined, register_form_show, undefined);
            };

            $scope.passwordReset = function ($event) {
                $event.preventDefault();
                utils.card_show_hide($login_card, undefined, password_reset_show, undefined);
            };
            $scope.Position1_option = ["Hotel", "Police"];

            $scope.Position1 = {
                plugins: {
                    'tooltip': ''
                },
                create: false,
                maxItems: 1,
                placeholder: 'Select...'
            };

            $scope.postdata = {
                username: "",
                password: "",

            }
            // var data =
            //     $localStorage.UserSession == undefined &&
            //         $localStorage.UserSession == undefined
            //         ? {}
            //         : $localStorage.UserSession;
            // if (Object.keys(data).length == 0) {
            //     $state.go("login");
            // }

            // $scope.save = function Login(username, password, data, token, $localStorage, response, $state) {

            //     $http({
            //         method: 'post',
            //         data: $scope.postdata,
            //         url: 'https://localhost:44303/users/authenticate'
            //     }).then(function (response) {
            //         window.localStorage.setItem('token', response.data.Token);
            //         if (response.data.roll == true) {
            //             $localStorage.UserSession = response.data.response[0];
            //             console.log($localStorage.UserSession, "User_session");

            //             //   if()
            //             $state.go("restricted.dashboard");
            //         } else {
            //             // UIkit.notify({
            //             //     message: response.data.response,
            //             //     // status: "danger",
            //             //     timeout: 3000,
            //             //     pos: "top-center"
            //             // });
            //             // $scope.content_preloader_hide();
            //         }
            //         console.log(response.data.Status, "reponse");
            //     });
            // };



            // $scope.save = function () {
            //     // alert("work");

            //     $http({
            //         method: 'Post',
            //         data: $scope.postdata,
            //         url: 'https://localhost:44303/users/authenticate'
            //     }).then(function mySuccess(response) {
            //         // console.log(response);
            //         window.localStorage.setItem('role', response.data.role);

            //         if (response.data.role == "Hotel") {



            //             $state.go("restricted.dashboard");
            //         }

            //         else {
            //             if (response.data.role == "Police") {

            //                 $state.go("restricted.policedashboard");
            //             }

            //         }

            //     }, function myError(response) {

            //     });
            // };

        }
    ]);
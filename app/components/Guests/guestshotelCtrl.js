angular
    .module('altairApp')
    .controller('GuestshotelCtrl',

        function($compile, $scope, $timeout, DTOptionsBuilder, DTColumnDefBuilder, $state, $http, $rootScope) {

            var vm = this;
            vm.dt_data = [];
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withDOM("<'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>>" +
                    "<'uk-overflow-container'tr>" +
                    "<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>")
                .withOption('createdRow', function(row, data, dataIndex) {
                    // Recompiling so we can bind Angular directive to the DT
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('headerCallback', function(header) {
                    if (!vm.headerCompiled) {
                        // Use this headerCompiled field to only compile header once
                        vm.headerCompiled = true;
                        $compile(angular.element(header).contents())($scope);
                    }
                })
                .withPaginationType('full_numbers')
                // Active Buttons extension
                .withColumnFilter({
                    aoColumns: [{
                            type: 'number',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'number',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        }, {
                            type: 'text',
                            bRegex: true,
                            bSmart: true
                        },




                    ]
                })

            .withButtons([

                    {
                        extend: 'excelHtml5',
                        text: '<i class="uk-icon-file-excel-o"></i> XLSX',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        },
                        titleAttr: ''
                    }, {
                        extend: 'pdfHtml5',
                        text: '<i class="uk-icon-file-pdf-o"></i> PDF',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        },
                        titleAttr: 'PDF'
                    }
                ])
                .withOption('initComplete', function() {
                    $timeout(function() {
                        $compile($('.dt-uikit .md-input'))($scope);
                    })
                });


            $scope.changeToggle = function(user, isToggle) {
                angular.forEach($scope.toggleModel, function(user) {
                    user.isToggle = false;
                });
                user.isToggle = isToggle;
            }

            $scope.Master=
                    [
                    {
                     
                      Name:"MARS HOTEL",
                      Address:"Opposite SIPCOT IT Park, Siruseri Old, Mahabalipuram, Near ECR Beach, Chennai, Tamil Nadu 603103. 044 6654 5454",
                     

                     },
                     ]

            $scope.viewData = [{
                Photo: "assets/img/personn.png",
                Name: "Hussain",
                Signature: "Hussain",
                PhoneNumber: "+917047760660",
                RoomNumber: "600",
                CheckInDate: "04-09-2018",
                CheckOutDate: "08-06-2018",
                Address: "Anna Nagar IT Park, Siruseri Old",
                IDProof: "assets/img/persons.png"

            }, 
            {
                Photo: "assets/img/person.png",
                Name: "Abraham ",
                Signature: "Abraham",
                PhoneNumber: "+917951674281",
                RoomNumber: "309",
                CheckInDate: "01-May-2019",
                CheckOutDate: "29-May-2019",
                Address: "Anna Nagar IT Park, Siruseri Old",
                IDProof: "assets/img/personn.png"

            },
            {
                Photo: "assets/img/persons.png",
                Name: "Farina",
                Signature: "Farina",
                PhoneNumber: "+919989376425",
                RoomNumber: "205",
                CheckInDate: "22-May-2018",
                CheckOutDate: "10-June-2019",
                Address: "Anna Nagar IT Park, Siruseri Old",
                IDProof: "assets/img/person.png"

            }, 
            {
                Photo: "assets/img/personn.png",
                Name: "Fayaz",
                Signature: "Fayaz",
                PhoneNumber: "+911031352600",
                RoomNumber: "108",
                CheckInDate: "23-May-2018",
                CheckOutDate: "27-May-2018",
                Address: "Anna Nagar IT Park, Siruseri Old",
                IDProof: "assets/img/persons.png"

            },
            {
                Photo: "assets/img/persons.png",
                Name: "Stephane",
                Signature: "Stephane",
                PhoneNumber: "+916187076370",
                RoomNumber: "101",
                CheckInDate: "01-May-2018",
                CheckOutDate: "05-May-2018",
                Address: "Anna Nagar IT Park, Siruseri Old",
                IDProof: "assets/img/person.png"

            }]

        }
    );
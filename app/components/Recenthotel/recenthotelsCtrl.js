angular
    .module('altairApp')
    .controller('recenthotelsCtrl',
        function ($scope, $rootScope, utils, $compile, $timeout, DTOptionsBuilder, DTColumnDefBuilder, $http, $state, $filter,
            $state) {
            var vm = this;
            vm.dtOptions = DTOptionsBuilder.newOptions()
                .withDOM(
                    "<'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>>" +
                    "<'uk-overflow-container'tr>" +
                    "<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>"
                )
                .withOption("createdRow", function (row, data, dataIndex) {

                    $compile(angular.element(row).contents())($scope);
                })
                .withPaginationType("full_numbers")
                .withColumnFilter({
                    aoColumns: [
                        {
                            type: "text",
                            bRegex: true,
                            bSmart: true
                        }, null,
                        {
                            type: "text",
                            bRegex: true,
                            bSmart: true
                        },
                        null,
                        null
                    ]
                })
                 $scope.Master=
                    [
                    {
                     
                      Name:"Ganesh",
                      AadharNo:"4156 0838 7103",
                      MobileNo:"+91 9884422215",
                      Address:"Opposite SIPCOT IT Park, Siruseri Old",

                     },
                     ]

            $scope.selectize_student_options = ["21178, Elisa Ojenda", "11532, Arture Rihani", "17897, Azusa Obara", "16190, Eri Sato", "17322, Amar"];

            $scope.selectize_student_config = {
                plugins: {

                },
                create: false,
                maxItems: 1,
                placeholder: 'Select Student'
            };



            $scope.selectize_class_options = ["General English ", "IELTS Preparation", 'Business English', 'Plus One(Elective)'];

            $scope.selectize_class_config = {
                plugins: {

                },
                create: false,
                maxItems: 1,
                placeholder: 'Select Class'
            };


            $scope.viewData = [

                {
                   
                    Image: "assets/img/Hotel1.jpg",
                    Name: "Gokulam Park",
                    Place: "Chennai",
                    CheckInDate: "15 May 19",
                    CheckOutDate: "16 May 19",

                }, {
                    
                    Image: "assets/img/Hotel2.jpg",
                    Name: "Ibis Chennai Sipcot",
                    Place: "Mumbai",
                    CheckInDate: "10 May 19",
                    CheckOutDate: "12 May 19",

                }, {
                    
                    Image: "assets/img/Hotel3.jpg",
                    Name: "Novotel chennai",
                    Place: "Banglore",
                    CheckInDate:"05 Apr 19",
                    CheckOutDate: "06 Apr 19",

                }, {
                    
                    Image: "assets/img/Hotel4.jpg",
                    Name: "Vivanta Chennai",
                    Place: "Chennai",
                    CheckInDate: "02 Feb 19",
                    CheckOutDate: "16 Feb 19",

                }
                , {
                    
                    Image: "assets/img/Hotel5.jpg",
                    Name: "Hotel Space",
                    Place: "Madurai",
                    CheckInDate:"03 Dec 18",
                    CheckOutDate: "05 Dec 18",

                }
            ];


        }
    );
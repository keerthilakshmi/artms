angular
    .module('altairApp')
    .controller('DashboardCtrl', [
        '$rootScope',
        '$scope',
        '$interval',
        '$timeout',
        'variables','utils', '$compile','DTOptionsBuilder',  'DTColumnDefBuilder', '$http', '$state', '$filter',
        function($rootScope, $scope, $interval, $timeout, variables,utils, $compile, DTOptionsBuilder, DTColumnDefBuilder, $http, $state, $filter) {
            
            var vm = this;

            // Information datatable configuration //

            vm.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM(
                "<'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>>" +
                "<'uk-overflow-container'tr>" +
                "<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>"
            )
            .withOption("createdRow", function(row, data, dataIndex) {
                $compile(angular.element(row).contents())($scope);
            })
            .withPaginationType("full_numbers")
            .withColumnFilter({
                aoColumns: [

                    {
                        type: "text",
                        bRegex: true,
                        bSmart: true
                    },
                    {
                        type: "text",
                        bRegex: true,
                        bSmart: true
                    },
                    null,
                    {
                        type: "text",
                        bRegex: true,
                        bSmart: true
                    },
                    {
                        type: "text",
                        bRegex: true,
                        bSmart: true
                    },
                    null

                ]
            })
             $scope.viewData = [{
                Image: "assets/img/personn.png",
                Name: "Hussain",
                PhoneNumber: "+917047760660",
                RoomNumber: "600",
                CheckInDate: "04-09-2018",
                CheckOutDate: "08-06-2018"
            }, 
            {
                Image: "assets/img/person.png",
                Name: "Abraham ",
                PhoneNumber: "+917951674281",
                RoomNumber: "309",
                CheckInDate: "01-May-2019",
                CheckOutDate: "29-May-2019"

            },
            {
                Image: "assets/img/persons.png",
                Name: "Farina",
                PhoneNumber: "+919989376425",
                RoomNumber: "205",
                CheckInDate: "22-May-2018",
                CheckOutDate: "10-June-2019"

            }, 
            {
                Image: "assets/img/personn.png",
                Name: "Fayaz",
                PhoneNumber: "+911031352600",
                RoomNumber: "108",
                CheckInDate: "23-May-2018",
                CheckOutDate: "27-May-2018"

            },
            {
                Image: "assets/img/person.png",
                Name: "Stephane",
                PhoneNumber: "+916187076370",
                RoomNumber: "101",
                CheckInDate: "01-May-2018",
                CheckOutDate: "05-May-2018"

            }]
          

           

            


            $scope.dynamicStats = [

                {
                    id: '2',
                    title: 'Guess in hotel now',
                    count: '0',
                    chart_data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
                    chart_options: {
                        height: 28,
                        width: 64,
                        fill: "#d1e4f6",
                        stroke: "#0288d1"
                    }
                }
                // {
                //     id: '3',
                //     titles: 'Total , 28-May-2019',
                //     count: '0',
                //     chart_data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
                //     chart_options: {
                //         height: 28,
                //         width: 64,
                //         fill: "#d1e4f6",
                //         stroke: "#0288d1"
                //     }
                // }


            ];

            // countUp update
            $scope.$on('onLastRepeat', function(scope, element, attrs) {

                $scope.dynamicStats[0].count = '380.00';
              

            });

            $scope.agenttype = 'agent';

            $scope.Master = [{
                    isActive: true,
                    InvoiceId: "143705",
                    InvoiceTotal: "$380.00",
                    InvoiceDate: "02-JUL-07",
                    Edit: "Edit,Invoice,Attributes",
                    Type: "Invoice",
                    Status: "Owing:$380.00",
                    Sent: "1",
                    Void: "",
                    Transfer: "",
                    Locked: "Locked,Request,Change",
                    GroupId: "",
                    Copy: "Copy"

                }, 
                {
                    isActive: false,
                    InvoiceId: "143705",
                    InvoiceTotal: "$380.00",
                    InvoiceDate: "02-JUL-07",
                    Edit: "Edit,Invoice,Attributes",
                    Type: "Invoice",
                    Status: "Owing:$380.00",
                    Sent: "1",
                    Void: "",
                    Transfer: "",
                    Locked: "Locked,Request,Change",
                    GroupId: "",
                    Copy: "Copy"
                }

            ]


            // Medical Insurance Information

            $scope.Medical = [{
                    Startdate: "01-MAR-19",
                    Enddate: "28-DEC-19",
                    Weeks: "4",
                    Createddate: "02-JUL-07",
                    Companyname: "Tugo",
                    Policynumber: "SCG 166342"
                   

                }, 
                {
                    Startdate: "28-DEC-18",
                    Enddate: "8-MAR-19",
                    Weeks: "4",
                    Createddate: "11-MAR-07",
                    Companyname: "Tugo",
                    Policynumber: "SCG 164898"
                }

            ]

            // Payment

            $scope.Payment = [
            {
                    Invoiceid: "143565",
                    Paymentdate: "14-MAR-19",
                    Paymentid: "94826",
                    Amount: "$320.00",
                    Method: "Debit",
                    Approved: "Y"
                   

                }, 
                {
                    Invoiceid: "Invoice Payment Total:",
                    Paymentdate: "",
                    Paymentid: "",
                    Amount: "$320.00",
                    Method: "",
                    Approved: ""
                },
                {
                    Invoiceid: "145660",
                    Paymentdate: "25-MAR-19",
                    Paymentid: "94662",
                    Amount: "$320.00",
                    Method: "Debit",
                    Approved: "Y"
                   

                }, 
                {
                    Invoiceid: "Invoice Payment Total:",
                    Paymentdate: "",
                    Paymentid: "",
                    Amount: "$320.00",
                    Method: "",
                    Approved: ""
                }

            ]

            // Course Information

             $scope.Course = [{
                    Course: "Course 101 - General English",
                    Startdate: "19-NOV-18",
                    Financialenddate: "21-DEC-18",
                    Numweeks: "5",
                    Note: "-"
                    
                   

                }, 
                {
                    Course: "Course 101 - General English",
                    Startdate: "19-MAR-19",
                    Financialenddate: "11-JAN-19",
                    Numweeks: "1",
                    Note: "-"
                    
                }

            ]

            $scope.Paid=[{
                Invoiced:"5",
                Coursename:"Course 105-Business English and General English",
                CourseInfo:"5",
                GivenCourse:"Course 105-Business English and General English"
            },
            {
                Invoiced:"10",
                Coursename:"Course 101- General English",
                CourseInfo:"5",
                GivenCourse:"Course 105-Business English and General English"
            }
            ]

            $scope.Purchase=[{
                ProductType:"Applications",
                TotalPaid:"$170.00"
            },
            {
            ProductType:"Tuition Fees",
                TotalPaid:"$4,480.00"
            },
            {
            ProductType:"TextBooks",
                TotalPaid:"$305.00"
            },
            {
            ProductType:"MI",
                TotalPaid:"$180.00"
            },
            {
            ProductType:"ID",
                TotalPaid:"$120.00"
            }
            ]
            

            // Classe Information
            $scope.Information = [{
                    Classid: "313",
                    Classname: "Pre-Advanced",
                    Classtype: "L/S",
                    Startdate: "02-JUL-07",
                    Enddate: "-",
                    Level: "5",
                    Weeksinclass: "-"
                   

                }, 
                {
                    Classid: "1089",
                    Classname: "Business English ",
                    Classtype: "BE",
                    Startdate: "02-JUL-07",
                    Enddate: "-",
                    Level: "5",
                    Weeksinclass: "-",
                   
                }

            ]


        }
    ]);